'use strict'
const Exame = use('App/Models/Exame')
const Event = use('Event')

class LaudarController {
  async update ({ request, response, params, auth }) {
    try {
      console.log(auth.user.person_id)
      const exams = await Exame.findByOrFail('id', params.id)
      console.log(`Logs-Laudo[${params.id}][Laudarcontroller-update]->carregou o exame`);     

      const data = request.only([
        'resultadoText',
        'resultadoType'
      ])
      const status = 3
      const dataResultado = new Date()
     

      exams.merge({ ...data, respLab_id: auth.user.person_id, dataResultado: dataResultado, status: status })
      console.log('exams ', JSON.stringify(exams))
      console.log(`Logs-Laudo[${params.id}][Laudarcontroller-update]->merge Dados, vai salvar`);

     const teste =  await exams.save()

     console.log('save: ', teste)
      console.log(`Logs-Laudo[${params.id}][Laudarcontroller-update]->depois de salvo`);


      return exams
    } catch (err) {
      console.log(err)
      return response
        .status(400)
        .send({ error: { message: 'Erro Ao Laudar Exame' } })
    }
  }

  async store({request, response, params}) {

    const exam = await Exame.findByOrFail('id', params.id)
    
    if(exam.status === 3) {
      Event.fire('new::laudo', exam.id)
    }

    return true

  }
}

module.exports = LaudarController
