'use strict'
const Exame = use('App/Models/Exame')
const Person = use('App/Models/Person')
const File = use('App/Models/File')
const Env = use('Env')
const Helpers = use('Helpers')

class ColetarController {
  async update ({ request, response, params, auth }) {
    try {
      const exams = await Exame.findByOrFail('id', params.id)

      const file = request.file('file', {
        types: ['image'],
        size: '5mb'
      })

      const name = `${new Date().getTime()}.${file.subtype}`
      await file.move(Helpers.publicPath('uploads'), {
        name: name
      })

      if (!file.moved()) {
        return file.error()
      }

      const filexam = await File.create({
        file: name,
        name: file.clientName,
        type: file.type,
        subtype: file.subtype
      })

      const dataColeta = new Date()
      const data = request.only([
        'paciente_id',
        'loteExame',
        'sintomas',
        'sintDesde',
        'febre',
        'fadiga',
        'coriza',
        'tosse',
        'faltadeAr',
        'dorGarganta',
        'dorCabeca',
        'perdaOlfato',
        'perdaPaladar',
        'calafrios',
        'dorCorpo',
        'diarreia',
        'contato10d'
      ])
      const status = 2
      const url = `${Env.get('APP_URL')}/files/${filexam.id}`
      const item = {}
      item.paciente_id = data.paciente_id
      item.loteExame = data.loteExame
      item.sintomas = data.sintomas
      item.febre = data.febre
      item.fadiga = data.fadiga
      item.coriza = data.coriza
      item.tosse = data.tosse
      item.faltadeAr = data.faltadeAr
      item.dorCabeca = data.dorCabeca
      item.perdaOlfato = data.perdaOlfato
      item.perdaPaladar = data.perdaPaladar
      item.calafrios = data.calafrios
      item.dorCorpo = data.dorCorpo
      item.dorCorpo = data.dorCorpo
      item.contato10d = data.contato10d
      if (data.sintomas == 1) {
        item.sintDesde = data.sintDesde
      }
      item.dataColeta = dataColeta

      exams.merge({ ...item, coletador_id: auth.user.person_id, status: status, fileExame: url })
      await exams.save()

      return exams
    } catch (err) {
      console.log(err)
      return response
        .status(400)
        .send({ error: { message: 'Erro Ao Coletar Exame', err } })
    }
  }
}

module.exports = ColetarController
