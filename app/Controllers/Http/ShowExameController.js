'use strict'
const Database = use('Database')

class ShowExameController {
  async show ({ params, response }) {
    try {
      // const results = await Resultado.all()

      const strgSQL = `select e.id, e."loteExame",e."dataValidadeExame",e.sintomas,e."sintDesde",e.febre,e.coriza,e.fadiga,
          e.tosse, e."faltadeAr", e."dorGarganta", e."dorCabeca", e."perdaOlfato",
          e."perdaPaladar", e.calafrios, e."dorCorpo", e.diarreia,
          e.contato10d, e."dataColeta",coalesce(e."resultadoText",'') as resultado, 
          e."dataColeta", e."dataResultado", t.nome as teste_nome,
            case  e.status
              when  0 then 'Vendido'
              when  1 then 'Associado'
              when  2 then 'Coletado'
              when  3 then 'Laudado'
              Else '...'
            end as statusText,
            e."fileExame", e."fileLaudo",
            p.nome as paciente_nome, p.doc, p.birth_date,
            p.psaude,
            p.pseg,
            p.pdrespcronica,
            p.drespcronica,
            p.pdcromossomica,
            p.dcromossomica,
            p.diabetes,
            p.imunossupressao,
            p.pdcardiaca,
            p.dcardiaca,
            p.gestanterisco,
            p.tempogestacao,
            p.obesidade,
            p.pdrenalcronica,
            p.drenalcronica
            
            From exames e
            left join people p on p.id=e.paciente_id
            left join testes t on t.id=e.teste_id  
            
            where e.id= '${params.id}'`
      const results = await Database
        .raw(strgSQL)

      const obj = {}
      obj.exame = results.rows[0]
      return obj
    } catch (err) {
      console.log(err)
      return response
        .status(400)
        .send({ error: { message: err } })
    }
  }
}

module.exports = ShowExameController
