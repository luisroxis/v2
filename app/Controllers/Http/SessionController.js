'use strict'
const User = use('App/Models/User')
const Person = use('App/Models/Person')
const { encrypt, decrypt } = require('../../utils/encrypt.js')

class SessionController {
  constructor () {
    try {
      const types = {
        typeRoot: false,
        typeAdministrativo: false,
        typeVendedor: false,
        typeEmpresa: false,
        typeMedico: false,
        typeColetador: false,
        typePaciente: false,
        typeLaboratorio: false,
        typeRespLab: false,
      }
    } catch (err) {
      console.log('erro em SessionControler.js - Store', new Date().toISOString, err)
    }
  }

  async store ({ request, response, auth }) {
    try {
      const { username, password } = request.all()

      

      if (username == null || username == '' || password == null || password == '') {
        return response.status(401)
          .send({ error: { message: 'Username ou Password não podem ser vazios' } })
      }

      const user = await User.findBy('username', username)
      this.types = {
        typeRoot: user.typeRoot,
        typeAdministrativo: user.typeAdministrativo,
        typeVendedor: user.typeVendedor,
        typeEmpresa: user.typeEmpresa,
        typeMedico: user.typeMedico,
        typeColetador: user.typeColetador,
        typePaciente: user.typePaciente,
        typeLaboratorio: user.typeLaboratorio,
        typeRespLab: user.typeRespLab,
        typeRevendedor: user.typeRevendedor
      }
      

      const token = await auth.attempt(username, password)
      const personRet = {
        nome: '',
        id: 0,
        idLab: 0,
        nameLab: ''
      }
      if (username === 'root') {
        personRet.nome = 'root'
      } else {
        const person = await Person.findBy('id', user.person_id)
        if (!person) {
          return response.status(400)
            .send({ error: { message: 'Erro no Login, Verificar Usuario ou Senha' } })
        }
        personRet.id = person.id
        personRet.nome = person.nome
        if (person.lab_id != null && person.lab_id > 0) {
          const personLab = await Person.findBy('id', person.lab_id)
          if (!personLab) {
            return response.status(400)
              .send({ error: { message: 'Erro no Login, Este Laboratório Não Existe' } })
          }
          personRet.idLab = personLab.id
          personRet.nameLab = personLab.nome
        }
      }
      const login = {}
      login.token = token
      login.user = { id: user.id, username: user.username }
      login.types = this.types
      login.person = personRet
      return login
    } catch (error) {
      console.log('erro em SessionControler.js - Store', new Date().toISOString(), error)
      return response.status(401)
        .send({ error: { message: 'Erro na API de Login' + error } })
    }
  }
}

module.exports = SessionController
